
//check if can access localstorage
if(typeof(Storage) == "undefined") {
  alert('Browser is not supported. Please use a modern browser.');
} else {
  if (localStorage.id) {document.getElementById("id").value = localStorage.id};
  if (localStorage.postcode) {document.getElementById("postcode").value = localStorage.postcode};
  ;

  //register service worker
  if ('serviceWorker' in navigator) { 
    try {
      //debug
      console.log('register SW')
      navigator.serviceWorker.register('./sw.js'); 
    } catch (e) {
      alert('ServiceWorker registration failed. Sorry about that.'); 
    }
  }
}



function isApple(){
  var uagent = navigator.userAgent.toLowerCase();
  return uagent.search("iphone") > -1 || uagent.search("ipad") > -1 || uagent.search("macintosh") > -1 ;
}

function createSMS() {
  var divider = '';

  if (isApple()){
      var divider = '&';
  }else{
      var divider = '?';
  };

  let phoneNum = '8998';
  let id = document.getElementById("id").value;
  let postcode = document.getElementById("postcode").value;
  let reason = document.querySelector('input[name="reason"]:checked').value;
  //set localstorage 
  localStorage.id=id;
  localStorage.postcode=postcode;	

  let message = reason + ' ' + id + ' ' + postcode;

  var url = "sms:" + phoneNum + divider + "body=" + encodeURIComponent(message);
  //alert(url);        
  window.location.href = url;

  //todo: manifest.. subdomain 

}
