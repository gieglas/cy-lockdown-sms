/*
Minor Version: 5
 */
const cacheName = 'cy-lockdown-sms-v3';

//install event
self.addEventListener('install', function(event) {
  //debug
  console.log('install');
  event.waitUntil(
    caches.open(cacheName)
      .then(function(cache) {
        cache.addAll([
          '.',
          'index.html',
          'favicon.ico',
          'manifest.json',
          'src/css/app.css',
          'src/js/app.js',
          'icon/android-icon-192x192-dunplab-manifest-28115.png',
          'icon/apple-icon-180x180-dunplab-manifest-28115.png',
          'icon/apple-icon-152x152-dunplab-manifest-28115.png',
          'icon/apple-icon-144x144-dunplab-manifest-28115.png',
          'icon/apple-icon-120x120-dunplab-manifest-28115.png',
          'icon/apple-icon-114x114-dunplab-manifest-28115.png',
          'icon/favicon-96x96-dunplab-manifest-28115.png',
          'icon/apple-icon-76x76-dunplab-manifest-28115.png',
          'icon/apple-icon-72x72-dunplab-manifest-28115.png',
          'icon/apple-icon-60x60-dunplab-manifest-28115.png',
          'icon/apple-icon-57x57-dunplab-manifest-28115.png',
          'icon/favicon-32x32-dunplab-manifest-28115.png',
          'icon/favicon-16x16-dunplab-manifest-28115.png',
          'icon/maskable_icon_512x512.png'
        ])
      })
  );
  return self.clients.claim();
});

//activate event
self.addEventListener('activate', event => {
  // delete any caches that aren't in expectedCaches
  // which will get rid of static-v1
  //debug
  console.log('activate');
  event.waitUntil(
    caches.keys().then(keys => Promise.all(
      keys.map(key => {
        if (!cacheName.includes(key)) {
          return caches.delete(key);
        }
      })
    )).then(() => {
      console.log('New version now ready to handle fetches!');
    })
  );
});

//fetch event
self.addEventListener('fetch', async event => {
  //debug
  console.log('fetch event for:', event.request.url)
	const req = event.request;
  event.respondWith(cacheFirst(req,navigator.onLine));
});

async function cacheFirst(req,isOffline) {
	const cache = await caches.open(cacheName); 
  const cachedResponse = await cache.match(req); 
  console.log(cachedResponse); 
  if (cachedResponse == undefined) {console.log('Undefined:',req);  }
	return cachedResponse || fetch(req); 
}
